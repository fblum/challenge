package com.fiftechfabo.challenge.challenge.ports;

import com.fiftechfabo.challenge.challenge.exceptions.WSException;
import com.fiftechfabo.challenge.challenge.models.WSResponse;

public interface WebServiceOutbound {
    public WSResponse getResponse() throws WSException;
}
