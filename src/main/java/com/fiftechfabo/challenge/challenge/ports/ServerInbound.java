package com.fiftechfabo.challenge.challenge.ports;

import com.fiftechfabo.challenge.challenge.exceptions.ServerException;

public interface ServerInbound {
    public void upload(String name) throws ServerException;
}
