package com.fiftechfabo.challenge.challenge.controllers;

import com.fiftechfabo.challenge.challenge.adapters.RESTWebServiceOutbound;
import com.fiftechfabo.challenge.challenge.adapters.SFTPServerInbound;
import com.fiftechfabo.challenge.challenge.exceptions.ServerException;
import com.fiftechfabo.challenge.challenge.exceptions.WSException;
import com.fiftechfabo.challenge.challenge.models.WSResponse;
import com.fiftechfabo.challenge.challenge.services.CSVUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;


@RestController
public class EmployeesController {
    @PostMapping("/")
    public String uploadFile() throws IOException, WSException, ServerException {
        RESTWebServiceOutbound rws = new RESTWebServiceOutbound();
        WSResponse e = rws.getResponse();
        CSVUtils csv = new CSVUtils(e.getEmployees());
        String fileName = "employees-" + System.currentTimeMillis()+".csv";
        csv.createCSV(fileName);
        SFTPServerInbound server = new SFTPServerInbound();
        server.upload(fileName);
        return "uploadCompleted";
    }
}
