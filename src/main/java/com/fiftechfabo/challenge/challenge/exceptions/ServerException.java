package com.fiftechfabo.challenge.challenge.exceptions;

public class ServerException extends Exception{
    public ServerException(String msg){
        super(msg);
    }
}
