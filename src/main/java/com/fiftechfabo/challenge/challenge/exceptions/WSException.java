package com.fiftechfabo.challenge.challenge.exceptions;

public class WSException extends Exception{
    public WSException(String msg){
        super(msg);
    }
}
