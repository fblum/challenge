package com.fiftechfabo.challenge.challenge.adapters;

import com.fiftechfabo.challenge.challenge.exceptions.WSException;
import com.fiftechfabo.challenge.challenge.models.WSResponse;
import com.fiftechfabo.challenge.challenge.ports.WebServiceOutbound;
import org.springframework.web.client.RestTemplate;

public class RESTWebServiceOutbound implements WebServiceOutbound {
    private static final String url = "http://dummy.restapiexample.com/api/v1/employees";

    public WSResponse getResponse() throws  WSException{
        RestTemplate restTemplate = new RestTemplate();
        try {
            return restTemplate.getForObject(url, WSResponse.class);
        }catch (Exception e){
            throw new WSException("The url " + url + " can't be reached");
        }
    }
}
