package com.fiftechfabo.challenge.challenge.adapters;


import java.io.IOException;
import java.io.InputStream;
import java.io.IOException;
import java.io.InputStream;

import com.fiftechfabo.challenge.challenge.exceptions.ServerException;
import com.fiftechfabo.challenge.challenge.ports.ServerInbound;
import org.springframework.integration.sftp.session.DefaultSftpSessionFactory;
import org.springframework.integration.sftp.session.SftpSession;
import java.io.ByteArrayOutputStream;


public class SFTPServerInbound implements ServerInbound {
    private static final String host = "35.239.166.66";
    private static final int port = 22;
    private static final String user = "sftp_user";
    private static final String passwd = "latterchangetocertificate";
    private static final String location = "/home/sftp_user/csvs/";

    private DefaultSftpSessionFactory getFactory(){
        DefaultSftpSessionFactory factory = new DefaultSftpSessionFactory();
        factory.setHost(host);
        factory.setPort(port);
        factory.setAllowUnknownKeys(true);
        factory.setUser(user);
        factory.setPassword(passwd);
        return factory;
    }

    public void upload(String name) throws ServerException {
        SftpSession session;
        InputStream resourceAsStream;
        try {
            session = getFactory().getSession();
            resourceAsStream =
                    SFTPServerInbound.class.getClassLoader().getResourceAsStream(name);
        }catch (Exception e){
            throw new ServerException("Couldn't connect with the given parameters");
        }
        try {
            session.write(resourceAsStream, location + name);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        session.close();
    }
}
